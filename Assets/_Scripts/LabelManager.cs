﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelManager : MonoBehaviour
{
    public LabelController[] labelControllers;

    public void ToggleAllLabels(bool enabled)
    {
        if(enabled == true)
        {
            foreach(LabelController l in labelControllers)
            {
                l.overrideLabel = false;
                l.EnableButton();
            }
        }
        else
        {
            foreach (LabelController l in labelControllers)
            {
                l.overrideLabel = true;
                l.DisableButton();
            }
        }
    }
}
