﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class LabelController : MonoBehaviour
{
    //PUBLIC VARIABLES - fields will appear in inspector and can be accessed by other classes
    //The name to display in the lable
    public string caption;
    //The game object to achor this label to
    public Transform target;
    //Override label controls
    public bool overrideLabel;


    //PRIVATE VARIABLES - fields will not appear in inspector and can only be accessed by this class
    //Variables to store some of the components attached to this object
    private RectTransform _rectTransform;
    private CanvasGroup _canvasGroup;
    private Text _captionText;

    // Start is called before the first frame update
    void Start()
    {
        //Get some of the components attached to this game object
        _rectTransform = GetComponent<RectTransform>();
        _canvasGroup = GetComponent<CanvasGroup>();
        _captionText = GetComponentInChildren<Text>(true); //find text components in child object and include them even if disabled

        //upate the caption text
        _captionText.text = caption;
    }

    // Update is called once per frame
    void Update()
    {
        //Don't update anything if the LabelManager disabled this object
        if(overrideLabel == true)
        {
            return; //don't execute any code after this line
        }

        //Update the position of this label so it follows a target object in world space
        _rectTransform.WorldToAnchoredPosition(target);

        //Check if the target object is being occluded...
        if(target.IsOccluded())
        {
            //...hide the label if occluded
            DisableButton();
        }
        else
        {
            //..otherwise make it visible
            EnableButton();
        }
    }

    //show the button and make it interactable
    public void EnableButton()
    {
        _canvasGroup.interactable = true;
        _canvasGroup.alpha = 1f;
    }

    //hide the button and make it inactive
    public void DisableButton()
    {
        _canvasGroup.interactable = false;
        _canvasGroup.alpha = 0f;
    }

    //this public method is called from the Button component on the label
    public void ToggleCaption()
    {
        bool isEnabled = _captionText.isActiveAndEnabled;
        _captionText.gameObject.SetActive(!isEnabled);
    }

}
