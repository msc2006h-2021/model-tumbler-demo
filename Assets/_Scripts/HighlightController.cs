﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightController : MonoBehaviour
{
    public Animator animator;
    private bool _isHighlighted;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseUp()
    {
        GameManager.Instance.Highlight(this);
        /*
        if(_isHighlighted == false)
        {
            AddHighlight();
        }
        else
        {
            RemoveHighlight();
        }
        */
    }

    public void AddHighlight()
    {
        if(_isHighlighted == false)
        {
            animator.SetTrigger("highlight");
            _isHighlighted = true;
        }
        
    }

    public void RemoveHighlight()
    {
        if(_isHighlighted == true)
        {
            animator.SetTrigger("default");
            _isHighlighted = false;
        }
    }

}
